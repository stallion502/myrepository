//
//  LaunchViewController.h
//  Realm
//
//  Created by Макс Позднышев on 12.02.17.
//  Copyright © 2017 Jay Ang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LaunchViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *mainLabel;

@end
