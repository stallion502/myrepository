//
//  LaunchViewController.m
//  Realm
//
//  Created by Макс Позднышев on 12.02.17.
//  Copyright © 2017 Jay Ang. All rights reserved.
//

#import "LaunchViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface LaunchViewController (){
    NSString *info;
    UIColor *mainColor;
}
@property (weak, nonatomic) IBOutlet UILabel *recordLabel;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;
@property (weak, nonatomic) IBOutlet UIButton *leftButton;

@property(nonatomic, strong) NSTimer *timer;

@end

@implementation LaunchViewController

#pragma mark - Button Methods

- (void)blinking{
    if( self.rightButton.layer.borderColor == [[UIColor clearColor] CGColor] ){
        self.rightButton.layer.borderColor = [mainColor CGColor];
    }
    else {
        self.rightButton.layer.borderColor = [[UIColor clearColor] CGColor];

    }
}


#pragma mark - Set Defaults 
- (void) setDefaults {
    mainColor = [UIColor colorWithRed:255.0f/255.0f green:76.0f/255.0f blue:95.0f/255.0f alpha:0.8];

}

- (void)changeLabelState:(NSTimer *)timer
{
    if(self.rightButton.layer.borderColor == [[UIColor blackColor]CGColor])
    {
        self.rightButton.layer.borderColor = [mainColor CGColor];
        [self performSelector:@selector(changeLabelState:) withObject:nil afterDelay:0.4];
    }
    else
    {
        self.rightButton.layer.borderColor = [[UIColor blackColor]CGColor];
        [self performSelector:@selector(changeLabelState:) withObject:nil afterDelay:0.8];
    }
}


- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setDefaults];
    
    self.rightButton.layer.borderWidth = 0.8f;
    self.leftButton.layer.borderWidth = 0.8f;
    
    self.rightButton.layer.borderColor = [[UIColor blackColor] CGColor];

    
    [self performSelector:@selector(changeLabelState:) withObject:nil afterDelay:0.4];
    
    
    info = @"Для ускорения ваших результатов заполните досье";
    self.recordLabel.alpha = 0;
    self.recordLabel.text = info;
    self.recordLabel.textColor = [UIColor whiteColor];
    self.mainLabel.textColor = [UIColor lightGrayColor];
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"Girl-Fitness.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    float newX = self.view.center.x;
    float newY = 60.0f;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [UIView transitionWithView:self.mainLabel
                          duration:0.5f
                           options:UIViewAnimationCurveEaseInOut
                        animations:^(void) {
                            self.mainLabel.center = CGPointMake(newX, newY);
                        }
                        completion:^(BOOL finished) {
                        }];
    });
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView transitionWithView:self.recordLabel
                          duration:1.0f
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            self.recordLabel.alpha = 1;
                            
                        } completion:nil];
    });
    
    
//   [UIView animateWithDuration:1.0f delay:1.5f options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse animations:^{
//        self.rightButton.layer.borderColor = [mainColor CGColor];
//    }completion:nil];
    self.timer = [NSTimer timerWithTimeInterval:0.4f target:self selector:@selector(blinking) userInfo:nil repeats:YES];

}

-(void)viewWillDisappear:(BOOL)animated{
    [self.timer invalidate];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
