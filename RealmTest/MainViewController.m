//
//  MainViewController.m
//  Realm
//
//  Created by Макс Позднышев on 15.03.17.
//  Copyright © 2017 Jay Ang. All rights reserved.
//

#import "MainViewController.h"
#import "LMDropdownView.h"
#import "FirstVariationViewController.h"
#import "UIView+MaterialDesign.h"

@interface MainViewController ()<LMDropdownViewDelegate, UIGestureRecognizerDelegate>{
    UIColor *mainColor;
    UIColor *firstColor;
    UIColor *secondColor;
}
@property (weak, nonatomic) IBOutlet UIView *firstlayout;
@property (weak, nonatomic) IBOutlet UIView *firstMidView;
@property (weak, nonatomic) IBOutlet UINavigationItem *navBar;
@property (strong, nonatomic)IBOutlet UIView *dropDownViewL;
@property (weak, nonatomic) IBOutlet UIView *secondMidView;
@property (weak, nonatomic) IBOutlet UIView *thirdMidView;
@property (weak, nonatomic) IBOutlet UIView *fouthMidView;

@property (strong, nonatomic) LMDropdownView *dropdownView;

@property (strong, nonatomic)UITapGestureRecognizer *fristTapGesture;
@property (assign) BOOL isTaped;


@end

@implementation MainViewController

- (void) setColors{
    //JustSomethingAdded
    mainColor = [UIColor colorWithRed:255.0f/255.0f green:76.0f/255.0f blue:95.0f/255.0f alpha:0.8];
    firstColor = [UIColor colorWithRed:245.0f/255.0f green:78.0f/255.0f blue:98.0f/255.0f alpha:0.8];
    secondColor = [UIColor colorWithRed:249.0f/255.0f green:76.0f/255.0f blue:104.0f/255.0f alpha:0.8];
    
}

#pragma mark - utilities 

- (void)taped{
    
//        CGPoint position = [self.fristTapGesture locationInView:self.firstlayout];
//
//        FirstVariationViewController *fvc = [[FirstVariationViewController alloc] init];
//
//    [UIView mdInflateTransitionFromView:self.view toView:fvc.view originalPoint:position duration:0.7 completion:nil];
    
    FirstVariationViewController *fvc = [[FirstVariationViewController alloc] init];
    
    CGPoint position = [self.fristTapGesture locationInView:self.firstlayout];
    
    [self.firstlayout mdInflateAnimatedFromPoint:position backgroundColor:[mainColor colorWithAlphaComponent:0.8f]
                                        duration:0.26f completion:^{
                                            [self.navigationController pushViewController:fvc animated:YES];
                                        }];
    
}

- (void)addGesture{
    //dasdasasd
    self.isTaped = FALSE;
    self.fristTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(taped)];
    [self.firstlayout addGestureRecognizer:self.fristTapGesture];
}

- (void)fillViews{
    
            UIGraphicsBeginImageContext(self.view.frame.size);
            [[UIImage imageNamed:@"Arnold-Schwarzenegger-Iphone-Wallpaper-Full-HD.png"] drawInRect:self.view.bounds];
            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            self.view.backgroundColor = [UIColor colorWithPatternImage:image];

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            self.view.layer.shadowColor = [[UIColor blackColor] CGColor];
            self.view.layer.shadowOffset = CGSizeMake(0,5);
            self.view.layer.shadowOpacity = 0.7;
            
            self.firstMidView.layer.shadowColor = [[UIColor blackColor] CGColor];
            self.firstMidView.layer.shadowOffset = CGSizeMake(0,5);
            self.firstMidView.layer.shadowOpacity = 0.7;
            
            self.secondMidView.layer.shadowColor = [[UIColor blackColor] CGColor];
            self.secondMidView.layer.shadowOffset = CGSizeMake(0,5);
            self.secondMidView.layer.shadowOpacity = 0.7;
            
            self.thirdMidView.layer.shadowColor = [[UIColor blackColor] CGColor];
            self.thirdMidView.layer.shadowOffset = CGSizeMake(0,5);
            self.thirdMidView.layer.shadowOpacity = 0.7;
            
            
            self.fouthMidView.layer.shadowColor = [[UIColor blackColor] CGColor];
            self.fouthMidView.layer.shadowOffset = CGSizeMake(0,5);
            self.fouthMidView.layer.shadowOpacity = 0.7;
            self.fouthMidView.layer.shadowRadius = 1.0;
            
            
            self.dropDownViewL.layer.cornerRadius = 5;
            self.dropDownViewL.layer.shadowOffset = CGSizeZero;
            self.dropDownViewL.layer.shadowOpacity = 0.7;
            self.dropDownViewL.layer.shadowRadius = 1.0;

    });
    
//    self.firstMidView.layer.shadowColor = [[UIColor blackColor] CGColor];
//    self.firstMidView.layer.shadowOffset = CGSizeMake(0,5);
//    self.firstMidView.layer.shadowOpacity = 0.5;

}
#pragma mark - DROPDOWN VIEW

- (void)showDropDownViewFromDirection:(LMDropdownViewDirection)direction // most important
{
    // Init dropdown view
    if (!self.dropdownView) {
        self.dropdownView = [LMDropdownView dropdownView];
        self.dropdownView.delegate = self;
        
        // Customize Dropdown style
        self.dropdownView.closedScale = 0.85;
        self.dropdownView.blurRadius = 5;
        self.dropdownView.blackMaskAlpha = 0.5;
        self.dropdownView.animationDuration = 0.5;
        self.dropdownView.animationBounceHeight = 20;
    }
    self.dropdownView.direction = direction;
    
    // Show/hide dropdown view
    if ([self.dropdownView isOpen]) {
        [self.dropdownView hide];
    }
    else {
                
                [self.dropdownView showFromNavigationController:self.navigationController
                                                withContentView:self.dropDownViewL];
            }
    
}



- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    self.dropDownViewL.frame = CGRectMake(CGRectGetMinX(self.dropDownViewL.frame),
                                           CGRectGetMinY(self.dropDownViewL.frame) + 60,
                                           CGRectGetWidth(self.view.bounds),
                                           CGRectGetHeight(self.dropDownViewL.bounds));
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
   // self.view = [[UIView alloc] initWithFrame:CGRectMake(0,0, 199, 119)];
//    UIView *myview = [[UIView alloc] initWithFrame:CGRectMake(0,0, 199, 119)];
//    myview.backgroundColor = [UIColor redColor];
 //   LMDropdownView *dropdownView = [LMDropdownView dropdownView];
 //   [dropdownView showFromNavigationController:self.navigationController withContentView:myview];
    [self.navBar.leftBarButtonItem setTintColor:[UIColor clearColor]];
    
}
- (IBAction)buttonTapped:(UIButton *)sender {
    [self showDropDownViewFromDirection:LMDropdownViewDirectionTop];
}

- (void)viewDidLoad {
    [super viewDidLoad];
        [self setColors];
        [self addGesture];
        
        [self fillViews];
    
    self.navigationItem.hidesBackButton = true;
//    self.navBar.hidesBackButton = true;

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.firstlayout.backgroundColor = [UIColor clearColor];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
