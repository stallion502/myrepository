//
//  ViewController.m
//  Realm
//
//  Created by VLT Labs on 6/4/15.
//  Copyright (c) 2015 Jay Ang. All rights reserved.
//

#import "ViewController.h"
#import "People.h"
#import <Realm.h>

@interface ViewController ()<UITextFieldDelegate>{
    UIColor *mainColor;
    UIColor *firstColor;
    UIColor *secondColor;
    
    NSString *PHmyName;
    NSString *PHmyWeight;
    NSString *PHmyChestVolume;
    NSString *PHwaistVolume; // Талия
    NSString *PHlegVolume;
    NSString *PHhipVolume;
    
    NSString *PHhipVolumeMale;
    
    NSString *myName;
    NSString *myWeight;
    
    NSArray *textfields;
    NSArray *names;
    NSArray *arrayViews;
    
    NSInteger currentTextfield;
    NSUInteger offset;
    NSInteger offsetDown;
}

@property (weak, nonatomic) IBOutlet UISegmentedControl *maleSegment;
@property (weak, nonatomic) IBOutlet UIButton *mainBut;

//@property (weak, nonatomic) IBOutlet UIView *firstView;
@property (weak, nonatomic) IBOutlet UIView *secondView;
@property (weak, nonatomic) IBOutlet UIView *thirdView;
@property (weak, nonatomic) IBOutlet UIView *fouthView;
@property (weak, nonatomic) IBOutlet UIView *fifthView;
@property (weak, nonatomic) IBOutlet UIView *sixthView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) UITapGestureRecognizer *tapGesture;
@property (nonatomic, strong) UIToolbar *numberToolBar;

@property RLMRealm *realm;
@end

@implementation ViewController

#pragma mark - Blur approaches 

- (void)dismiss {

    [self.waist resignFirstResponder];
    [self.name resignFirstResponder];
    [self.leg resignFirstResponder];
    [self.armsHip resignFirstResponder];
    [self.chestVolume resignFirstResponder];
    [self.weight resignFirstResponder];
    
}

#pragma mark - TextField Delegate Methods 

-(void)textFieldDidBeginEditing:(UITextField *)textField { //Keyboard becomes visible

    for (int i = 0; i < textfields.count; i++) {
        if ([[textfields objectAtIndex:i] isEqual:textField]) {
            currentTextfield = i;
        }
    }
    //perform actions.
}



#pragma mark - NumberPad features

- (void)somethingThatSouldNotWork{
    self.numberToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    self.numberToolBar.barStyle = UIBarStyleBlackTranslucent;
    self.numberToolBar.items = @[
                            [[UIBarButtonItem alloc]initWithTitle:@"Назад" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc] initWithTitle:@"Далее" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    
    [[UIBarButtonItem appearance] setTintColor:[UIColor lightGrayColor]];
    
    [self.numberToolBar sizeToFit];
    
 //   self.name.inputAccessoryView = self.numberToolBar;
    self.chestVolume.inputAccessoryView = self.numberToolBar;
    self.weight.inputAccessoryView = self.numberToolBar;
    self.armsHip.inputAccessoryView = self.numberToolBar;
    self.leg.inputAccessoryView = self.numberToolBar;
    self.waist.inputAccessoryView = self.numberToolBar;
    
}

- (void) segmentedControl{
    
    
    if (self.maleSegment.selectedSegmentIndex == 0) //salty
    {
        self.armsHip.layer.borderWidth = 2.0f;
        
        NSAttributedString *str = [[NSAttributedString alloc] initWithString:PHhipVolume attributes:@{ NSForegroundColorAttributeName : mainColor }];
        [self.armsHip setAttributedPlaceholder:str];
        [self.armsHip.layer setBorderColor:[mainColor CGColor]];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    NSAttributedString *str = [[NSAttributedString alloc] initWithString:PHhipVolume attributes:@{ NSForegroundColorAttributeName : [UIColor lightGrayColor] }];
            [self.armsHip setAttributedPlaceholder:str];
            self.armsHip.layer.borderWidth = 0.0f;

            
        });
        
    }
    else if (self.maleSegment.selectedSegmentIndex == 1) //sweet
    {
        
        self.armsHip.layer.borderWidth = 2.0f;
        
        NSAttributedString *str = [[NSAttributedString alloc] initWithString:PHhipVolumeMale attributes:@{ NSForegroundColorAttributeName : mainColor}];
        [self.armsHip setAttributedPlaceholder:str];
        [self.armsHip.layer setBorderColor:[mainColor CGColor]];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.armsHip.layer.borderWidth = 0.0f;
                    NSAttributedString *str = [[NSAttributedString alloc] initWithString:PHhipVolumeMale attributes:@{ NSForegroundColorAttributeName : [UIColor lightGrayColor] }];
            [self.armsHip setAttributedPlaceholder:str];

            
        });
    }
}

- (void)cancelNumberPad{
    
    if (currentTextfield == 0) {
        return;
    }
    
    if (!CGPointEqualToPoint(self.scrollView.contentOffset, CGPointZero)) {
        CGPoint scrolloffsetOne = self.scrollView.contentOffset;
        [self.scrollView setContentOffset:scrolloffsetOne animated:YES];
        scrolloffsetOne.y -= offsetDown;
        
    }
    [(UITextField *)[textfields objectAtIndex:currentTextfield - 1] becomeFirstResponder];
    
}

- (void)doneWithNumberPad{
    
    
    if (currentTextfield < textfields.count){
        
        if (currentTextfield == textfields.count - 1) {
            offset = 50;
            [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
            [(UITextField *)[textfields objectAtIndex:0] becomeFirstResponder];
            return;
        }


    CGPoint scrolloffset = self.scrollView.contentOffset;
    scrolloffset.y += offset;
    [self.scrollView setContentOffset:scrolloffset animated:YES];

    [(UITextField *)[textfields objectAtIndex:currentTextfield + 1] becomeFirstResponder];
        
    }
    
}

-(UIImage *)imageWithView:(UIView *)view{
    
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


- (void)keyBoardWillShow {
    CGPoint touchPoint = [self.tapGesture locationInView:self.view]; // 220 - for different type of current device
    NSInteger currentValue;
    NSInteger mainValue;
    if ([[UIScreen mainScreen] nativeBounds].size.height == 960) {
        currentValue = 230;
        mainValue = 230;
    }
    else if ([[UIScreen mainScreen] nativeBounds].size.height == 1136){
        currentValue = 250;
        mainValue = 250;

    }
    else if ([[UIScreen mainScreen] nativeBounds].size.height == 1136){
        currentValue = 250;
        mainValue = 250;
    }
    else if ([[UIScreen mainScreen] nativeBounds].size.height == 2208){
        currentValue = 400;
        mainValue = 280;
    }
    else{
        currentValue = 250;
        mainValue = 250;
    }
    
    NSInteger value =  touchPoint.y - currentValue;
    if (touchPoint.y  > self.view.frame.size.height - mainValue) {
        [UIView animateWithDuration:.25 animations:^{
            [self.scrollView setContentOffset:CGPointMake(0, value) animated:YES];
        }];
    }

}

- (void)keyboardWillHide{
     [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];

}

- (void)blurIt{

    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = self.mainBut.bounds;
    gradient.colors = @[(id)mainColor.CGColor, (id)firstColor.CGColor, (id)secondColor.CGColor];
    
    [self.mainBut.layer insertSublayer:gradient atIndex:0];}

- (void)makeView{
    
    for (int i = 0; i < arrayViews.count; i++) {
       // [(UIView *)[arrayViews objectAtIndex:i] setBackgroundColor:[UIColor ma]];
        [(UIView *)[arrayViews objectAtIndex:i] setAlpha:0.1f];
        [[(UIView *)[arrayViews objectAtIndex:i] layer] setCornerRadius:11.0f];
        [[(UIView *)[arrayViews objectAtIndex:i] layer] setBorderWidth:0.0f];

    }
}

#pragma mark - Textfield Delegates Methods

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];

    return YES;
}

- (void) setColors{
    
    mainColor = [UIColor colorWithRed:255.0f/255.0f green:76.0f/255.0f blue:95.0f/255.0f alpha:0.8];
    firstColor = [UIColor colorWithRed:245.0f/255.0f green:78.0f/255.0f blue:98.0f/255.0f alpha:0.8];
    secondColor = [UIColor colorWithRed:249.0f/255.0f green:76.0f/255.0f blue:104.0f/255.0f alpha:0.8];
    
}

- (void) setDefaults {
    
    textfields = @[self.weight, self.chestVolume, self.waist, self.leg, self.armsHip];
    
    arrayViews = @[self.secondView, self.thirdView, self.fouthView, self.fifthView, self.sixthView];
    
  //  PHmyName = @"Ваше имя";
    PHmyWeight = @"Ваш вес";
    PHmyChestVolume = @"Обхват груди";
    PHwaistVolume = @"Обхват талии";
    PHlegVolume = @"Обхват голени";
    PHhipVolume = @"Обхват рук";
    
    PHhipVolumeMale = @"Обхват бедер";
    
    names = @[PHmyWeight, PHmyChestVolume, PHwaistVolume, PHlegVolume, PHhipVolume];
    
    [self.maleSegment setTitleTextAttributes:@{
                                              NSFontAttributeName:[UIFont fontWithName:@"PF Agora Slab Pro" size: 20.0f],
                                              NSForegroundColorAttributeName:[UIColor lightGrayColor]}
                                    forState:UIControlStateNormal];
    
    self.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
    
    self.mainBut.layer.borderWidth = 1.0f;
    self.mainBut.layer.borderColor = [[UIColor blackColor] CGColor];
    
    offset = 50;
    offsetDown = -40;
    
    [self.maleSegment addTarget:self action:@selector(segmentedControl) forControlEvents:UIControlEventValueChanged];
    
    [self.view addGestureRecognizer:self.tapGesture];
}

#pragma mark - Set TextField Defaults 
- (void) setTextFieldDefaultsWithColor:(UIColor *)color withFontColor:(UIColor *)FC {
    
    for (int i = 0; i < [textfields count]; i++) {
        [(UITextField *)[textfields objectAtIndex:i] layer].cornerRadius = 11.0f;
        [(UITextField *)[textfields objectAtIndex:i] layer].borderWidth = 0.0f;

        [(UITextField *)[textfields objectAtIndex:i] setFont:[UIFont fontWithName:@"PF Agora Slab Pro" size: 25.0f]];
            [(UITextField *)[textfields objectAtIndex:i]  setValue:[UIFont fontWithName: @"PF Agora Slab Pro" size: 25.0f] forKeyPath:@"_placeholderLabel.font"];
        
            NSAttributedString *str = [[NSAttributedString alloc] initWithString:[names objectAtIndex:i] attributes:@{ NSForegroundColorAttributeName : FC }];
        [(UITextField *)[textfields objectAtIndex:i] setAttributedPlaceholder:str];
        
    }
}


#pragma mark - View Delegate Methods

- (void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow) name:UIKeyboardWillShowNotification object:nil];
    
    [self setColors];

    [self setDefaults];
    
    [self setTextFieldDefaultsWithColor:[UIColor lightGrayColor] withFontColor:[UIColor lightGrayColor]];
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"Girl-Fitness.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    [self makeView];
    
 //   [self blurIt];
    

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.navigationItem.leftBarButtonItem = nil;

    [self somethingThatSouldNotWork];
    
    self.weight.delegate = self;
    self.weight.autocorrectionType = UITextAutocorrectionTypeNo;
    self.weight.keyboardType = UIKeyboardTypeNumberPad;

    self.chestVolume.delegate = self;
    self.chestVolume.autocorrectionType = UITextAutocorrectionTypeNo;
    self.chestVolume.keyboardType = UIKeyboardTypeNumberPad;

  //  self.name.delegate = self;
  //  self.name.autocorrectionType = UITextAutocorrectionTypeNo;
    
    self.leg.delegate = self;
    self.leg.autocorrectionType = UITextAutocorrectionTypeNo;
    self.leg.keyboardType = UIKeyboardTypeNumberPad;

    self.waist.delegate = self;
    self.waist.autocorrectionType = UITextAutocorrectionTypeNo;
    self.waist.keyboardType = UIKeyboardTypeNumberPad;

    self.armsHip.delegate = self;
    self.armsHip.autocorrectionType = UITextAutocorrectionTypeNo;
    self.armsHip.keyboardType = UIKeyboardTypeNumberPad;

    
  //  self.realm = [RLMRealm defaultRealm];
    

  //  [self savingPeopleViaRealm];
 //   [self loadPeople];
  //  [self deletePeople];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

-(void)savingPeopleViaRealm
{
    People *people = [People new];
 //   people.myName = self.name.text;
    people.myWeight = self.weight.text;
    people.myChestVolume = self.chestVolume.text;
    people.waistVolume = self.waist.text;
    people.legVolume = self.leg.text;
    people.hipVolume = self.armsHip.text;
    
    switch (self.maleSegment.selectedSegmentIndex) {
        case 0:
            people.sex = 0;
            break;
        case 1:
            people.sex = 0;
            break;
    }
    

        [self.realm beginWriteTransaction];
        [self.realm addObject:people];
        [self.realm commitWriteTransaction];
}

-(void)loadPeople
{
//    RLMResults *queryResults = [People allObjects];
//    NSLog(@"queryResults: \r %@", queryResults);
//    
//    if (queryResults != nil)
//    {
//        for (People *thesePeople in queryResults) {
//            NSLog(@"thesePeople: %@", thesePeople);
//         //   self.nameLabel.text = thesePeople.name;
//          //  self.ageLabel.text = thesePeople.age;
//        }
//
//    }
}

-(void)deletePeople
{
//    RLMResults *people = [People objectsWhere:@"name == 'Piddolo'"];
//    
//    if (people != nil)
//    {
//        [self.realm beginWriteTransaction];
//        [self.realm deleteObjects:people];
//        [self.realm commitWriteTransaction];
//    }
}

@end
