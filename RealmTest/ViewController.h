//
//  ViewController.h
//  Realm
//
//  Created by VLT Labs on 6/4/15.
//  Copyright (c) 2015 Jay Ang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *weight;
@property (weak, nonatomic) IBOutlet UITextField *chestVolume;
@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UITextField *waist;//обхват талии
@property (weak, nonatomic) IBOutlet UITextField *armsHip; // руки - бедра
@property (weak, nonatomic) IBOutlet UITextField *leg;
@property (weak, nonatomic) IBOutlet UIView *preView;

@end

