//
//  People.h
//  Realm
//
//  Created by VLT Labs on 6/4/15.
//  Copyright (c) 2015 Jay Ang. All rights reserved.
//

#import "RLMObject.h"

@interface People : RLMObject
@property NSString *myName;
@property NSString *myWeight;
@property NSString *myChestVolume;
@property NSString *waistVolume; // Талия
@property NSString *legVolume;
@property NSString *hipVolume;

@property (nonatomic, assign)BOOL sex;

@end
