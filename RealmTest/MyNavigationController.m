//
//  MyNavigationController.m
//  Realm
//
//  Created by Макс Позднышев on 17.03.17.
//  Copyright © 2017 Jay Ang. All rights reserved.
//

#import "MyNavigationController.h"

@implementation MyNavigationController

- (void)viewDidLoad{
    [super viewDidLoad];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, self.navigationBar.bounds.size.height - 2, self.navigationBar.bounds.size.width, 2)];
    lineView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    lineView.backgroundColor = [[UIColor darkGrayColor] colorWithAlphaComponent:0.2];
    [self.navigationBar addSubview:lineView];
    [self.navigationBar bringSubviewToFront:lineView];
}

@end
